// Author :  Alessandro Mirone ESRF 2011
// ==============================================================
// COMPILATION :nvcc -arch=sm_20 medianstack.cu -lcudart
// =======================================================

#include <stdio.h>
#include <stdlib.h>
#include<math.h>

#include <cuda.h>
#include <fcntl.h>
#include<iostream>

#define DODEBUG 1

#define DEBUG( call  ) if(DODEBUG) call ;



#define CUDA_SAFE_CALL_EXCEPTION(call) {do {	\
       cudaError err = call;    \
       if( cudaSuccess != err) {   \
	   sprintf(messaggio, "Cuda error in file '%s' in line %i  function %s : %s\n", \
                   __FILE__, __LINE__, __PRETTY_FUNCTION__, cudaGetErrorString( err) );	\
           fprintf(stderr, messaggio );     \
	   goto fail;			    \
       } } while (0); }

// -----------------------------------------------------------------------------------------------




// estimation of shared size  : 1 +  2*DIMBLOCK*( ((nimages-1)/2) ) + 2*(DIMBLOCK-1) +1  

template<int shared_size>
__global__ static void    cuda_medianstack_kernel(int  nimages, 
						  int isize_padded   ,
						  unsigned short* buffer,
						  unsigned short* target_buffer  
						  ) {
  __shared__   unsigned  short  data[ shared_size ] ; 
  unsigned short tmp, prefetched;
  const  int  bidx = blockIdx.x;
  const  int  tidx = threadIdx.x;
  const  int  bdim =  blockDim.x ;
  

#define DATA(i)   data[ (i)%2 +  2*bdim*( (i)/2 ) + tidx*2     ] 
  int i_im=0; 
  prefetched = buffer[   bdim* bidx +      tidx+           i_im* isize_padded   ] ; 



  for( i_im=0; i_im<nimages; i_im++) {

    DATA(i_im)= prefetched;

    syncthreads();

    if( i_im<nimages-1) {
      prefetched = buffer[   bdim* bidx +      tidx+           (i_im+1)* isize_padded   ] ; 
    }
    
    for(int k=i_im; k>0; k--) {
      if( DATA(k)<DATA(k-1) ) {
	tmp =  DATA(k)  ; 
	DATA(k)=DATA(k-1);
	DATA(k-1) = tmp ; 
      }
    } 
  }
  syncthreads();
  
  target_buffer[   bdim* bidx +      tidx  ]  =   DATA( (nimages/2)) ;
}  




typedef struct cuda_medianstack{
  int isize, isize_padded;
  int nimages ; 
  int index ; 
  int completeness ; 
  unsigned short * buffer;
  unsigned short * target_buffer;
}  cuda_medianstack;


void  cuda_medianstack_finish( cuda_medianstack* self    ) {
  char messaggio[10000];   
  CUDA_SAFE_CALL_EXCEPTION(cudaFree ( self->buffer ) );
 fail :     
  throw messaggio ;
}
void  cuda_medianstack_getMedian(  cuda_medianstack* self,   unsigned short * image_res ) {
  char messaggio[10000];   
  if(!self->completeness) {
    sprintf(messaggio, " problem : you asked a median befores filling all the buffers in file '%s' in line %i  \n",   __FILE__, __LINE__ );
    throw messaggio;
  }
  int tileX = 256  ; 
  dim3 dimGrid (  self->isize_padded/tileX + (( self->isize_padded % tileX   )>0)  );
  dim3      dimBlock (  tileX    );




  float cuTime_ms;
  cudaEvent_t event_s,event_e;
  cudaEventCreate(&event_s);
  cudaEventCreate(&event_e);

  cudaEventRecord(event_s);

  // estimation of shared size  : 1 +  2*DIMBLOCK*( ((nimages-1)/2) ) + 2*(DIMBLOCK-1) +1  
  int estimation =  1 +  2*tileX*( ((self->nimages-1)/2) ) + 2*(tileX-1) +1  ;

  if( estimation<=1024) {
    cuda_medianstack_kernel<1024><<<dimGrid,dimBlock>>> ( self->nimages, self->isize_padded   , self->buffer, self->target_buffer  );
  } else if( estimation<=2048) {
    cuda_medianstack_kernel<2048><<<dimGrid,dimBlock>>> ( self->nimages, self->isize_padded   , self->buffer, self->target_buffer  );
  } else if( estimation<=4096) {
    cuda_medianstack_kernel<4096><<<dimGrid,dimBlock>>> ( self->nimages, self->isize_padded   , self->buffer, self->target_buffer  );
  }  // else {
  //   sprintf(messaggio, "  You have to recompile with a larger shared memory " );
  //   goto fail;
  // }




else if( estimation<=8192) {
    cuda_medianstack_kernel<8192><<<dimGrid,dimBlock>>> ( self->nimages, self->isize_padded   , self->buffer, self->target_buffer  );
  } else if( estimation<=8192*2) {
    cuda_medianstack_kernel<16384 ><<<dimGrid,dimBlock>>> ( self->nimages, self->isize_padded   , self->buffer, self->target_buffer  );
  } else {
    sprintf(messaggio, "  You have to recompile with a larger shared memory " );
    goto fail;
  }


  
  cudaEventRecord(event_e);
  cudaEventSynchronize(event_s);
  cudaEventSynchronize(event_e);
  cudaEventElapsedTime(&cuTime_ms,event_s,event_e);
  std::cout << __FUNCTION__ << " elapsed time: " << cuTime_ms << " (ms) " << std::endl;
  cudaEventDestroy(event_e);
  cudaEventDestroy(event_s);


  CUDA_SAFE_CALL_EXCEPTION(cudaMemcpy ( image_res ,  self->target_buffer , 
					self->isize*sizeof(unsigned short) , cudaMemcpyDeviceToHost ) );
  return ;
 fail :     
  throw messaggio ;
}
void  cuda_medianstack_addImage(  cuda_medianstack* self,   unsigned short * image ) {
  char messaggio[10000];   
  CUDA_SAFE_CALL_EXCEPTION(cudaMemcpy ( self->buffer  +self->index* self->isize_padded  , image ,
					 self->isize_padded*sizeof(unsigned short) , cudaMemcpyHostToDevice ) );
  self->index=self->index+1;
  
  if(self->index==self->nimages) {
    self->index=0;
    self->completeness=1;
  }
  return  ; 
 fail :     
  throw messaggio ;
}



cuda_medianstack *cuda_medianstack_init( int  Nimages,  int  size_image )   {  
  cuda_medianstack * res = new  cuda_medianstack ; 
  res->nimages = Nimages;
  res->isize = size_image;
  res->isize_padded =  32*ceil(size_image/32.0);
  res->index = 0;
  res->completeness=0;
  res->buffer=0;
  res->target_buffer=0;
  
  char messaggio[10000];   
  
  int dev;
  cudaDeviceProp  prop;
  CUDA_SAFE_CALL_EXCEPTION( cudaGetDevice(&dev));
  DEBUG(printf(" il device utilizzato est %d \n", dev );)
    
    CUDA_SAFE_CALL_EXCEPTION(cudaGetDeviceProperties( &prop,dev));

  DEBUG(printf("CHOSEN DEVICE %s  \n", prop.name ));
  DEBUG(printf(" la shared mem per block est %d ( bytes)\n", prop.sharedMemPerBlock););
  DEBUG(printf(" la totalConstMem;  est %d ( bytes)\n", prop.totalConstMem););
  
  CUDA_SAFE_CALL_EXCEPTION( cudaMalloc((void**)&res->buffer  ,  sizeof(unsigned short) *res->nimages* res->isize_padded ));
  CUDA_SAFE_CALL_EXCEPTION( cudaMalloc((void**)&res->target_buffer  ,  sizeof(unsigned short) * res->isize_padded ));
  
  return res; 
 fail :     
  if(res->buffer   ) cudaFree(res->buffer );
  if(res->target_buffer   ) cudaFree(res->target_buffer );
  throw messaggio ;
}


main() {
  
  struct cuda_medianstack * stack; 
  int Nimages=21;
  int size_image=2048*2048; 
  unsigned short * image = new unsigned short [size_image] ; 
  unsigned short * res_image = new unsigned short [size_image] ; 
  
  stack =  cuda_medianstack_init(  Nimages,   size_image ) ;

  for(int i=0; i< Nimages; i++) {
    for(int k=0; k<size_image; k++) {
      image[k] = (i*23)%Nimages ; 
    }
    try {
      printf("%d ", image[0]);
      cuda_medianstack_addImage( stack, image);
    } catch (char *message) {
      std::cout << " I received this exception message " << message << std::endl ; 
      exit(0);
    }
  }
  printf("\n launching median \n");
  for(int volta=0; volta<100; volta++) {
    try {
       for(int k=0; k<size_image; k++) {
    	  image[k] = volta%21 ; 
       }	  
      cuda_medianstack_addImage( stack, image);
      cuda_medianstack_getMedian(  stack, res_image);
    } catch (char *message) {
      std::cout << " I received this exception message " << message << std::endl ; 
      exit(0);
    }
    printf(" RESULTS %d %d %d \n" , res_image[0], res_image[132], res_image[2048*1000]);
  }
}
