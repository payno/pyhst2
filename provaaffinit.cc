
#/*##########################################################################
# Copyright (C) 2001-2013 European Synchrotron Radiation Facility
#
#              PyHST2  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# PyHST2 is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for PyHST2: Alessandro Mirone.
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# PyHST2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PyHST2; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PyHST2 follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/
#include<stdio.h>
# include <unistd.h>
# include <sched.h>


int hw_sched_get_cpu_count(void) {
    int err;

    int cpu_count;
    cpu_set_t mask;

    err = sched_getaffinity(getpid(), sizeof(mask), &mask);
    if (err) return 1;

    printf("%ld\n", mask);



# ifdef CPU_COUNT

    cpu_count = CPU_COUNT(&mask);
    printf(" cpu_count %d \n",cpu_count );
# else
    for (cpu_count = 0; cpu_count < 24 ; cpu_count++) {
      printf("cpu_count %d %d  \n ", cpu_count, CPU_ISSET(cpu_count, &mask));
      // if (!CPU_ISSET(cpu_count, &mask)) break;
    }
    CPU_ZERO( &mask)  ;
    CPU_SET( 2 , &mask);
    sched_setaffinity(getpid(), sizeof(mask), &mask);
    err = sched_getaffinity(getpid(), sizeof(mask), &mask);
    if (err) return 1;
    for (cpu_count = 0; cpu_count < 24 ; cpu_count++) {
      printf("cpu_count %d %d \n ", cpu_count, CPU_ISSET(cpu_count, &mask));
      // if (!CPU_ISSET(cpu_count, &mask)) break;
    }


# endif

    if (!cpu_count) cpu_count = 1;
    return cpu_count;    

}

main() {
  int res= hw_sched_get_cpu_count();
  printf("res %d \n", res);
  while(1);
}
