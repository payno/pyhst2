import time
import numpy
#import random
from numpy import *
from numpy.fft import ifft
from pylab import *
import matplotlib.pyplot as plt

import moduleJL2 as JL2

N = 40
A = JL2.gen_base(N)
[source,b] = JL2.gen_image(N,0.1)
P1 = JL2.Problem(A,b,0.5,0.01,source)
#P1.niter(40)
a=time.clock()
P1.fista_n(False,True)
b=time.clock()
P1.PR_CG()
c=time.clock()
P1.CP()
d = time.clock()
print("FISTA : ",str(b-a))
print("PR_CG : ",str(c-b))
print("CP : ",str(d-c))
x = arange(P1.niter_CG+1)
plot(x,P1.err_F,'r',label='FISTA')
plot(x,P1.err_CG,'g',label='PR-CG')
plot(x,P1.err_CP,'c',label='CP')
legend()
show()
#P1.graph()
