import time
import numpy
#import random
from numpy import *
from numpy.fft import ifft
from pylab import *
import matplotlib.pyplot as plt

import moduleJL2 as JL2

N = 40
A = JL2.gen_base(N)
[source,b] = JL2.gen_image(N,0.1)
rho = linspace(0.1,1,10)
s = linspace(0.01,0.1,10)
e_FISTA = zeros(10)
e_PRCG = zeros(10)
e_CP = zeros(10)
for i in range(10):
    P1 = JL2.Problem(A,b,rho[i],0.01,source)
    P1.fista_n(False,True)
    e_FISTA[i] = P1.err_F[30]
    P1.PR_CG()
    e_PRCG[i] = P1.err_CG[30]
    P1.CP()
    e_CP[i] = P1.err_CP[30]
    show()
plot(rho,e_FISTA,'r',label='FISTA')
plot(rho,e_PRCG,'g',label='PR-CG')
plot(rho,e_CP,'c',label='CP')
ylim(0,1)
title('Analyse de l''erreur apres 30 iterations - sigma = 0.1 - s = 0.01')
xlabel("s")
ylabel("erreur")
show()
