"""
The data provided with these dataset have been kindly made
available by several ESRF scientists.
Thanks to Paul , Wolfgang and Paola .

If you reuse those provided in  PATCHES_VECTORIAL
you are kindly asked to aknoweledge the following
collaboration

"We acknowledge the German Excellence cluster "Munich-centre for Advanced Photonics" (EXC158), 
the Department of Clinical Radiology of the Ludwig Maximilians University (Munich) and 
the Biomedical beamline of the ESRF (Grenoble) for the provision of experimental data."


To run all them  all you have to do is
python nonregression.py

provided that you did the following , beforehand :

 you have to change three prefix,
 the lanching instruction for pyhst2 (LAUNCHING_INSTRUCTION),
 and optinally set the mailserver variable of you want to be notified by mail of the tasks end:

The prefixs :
   
  --- outputprefix : the several reconstructed cases, you will find them in an arborescence
      starting at outputprefix

  --- inputprefix : all the input.par files in the arborescence below inputprefix will be runned
 
  --- PREFIX  : this is where the data set are.

The available cases are

CASI = [ "NNFBP","CRAYON" , "MULTIPAGANIN", "PATCHES_VECTORIAL", "LENA_MULTIRINGS", "ID11_SNOW", "SIRT_LENA" ]

You can run shorter tests by shortening such list.

"""
mailserver = None
# mailserver="tuodomain.country"

# LAUNCHING_INSTRUCTION  = "echo 'localhost\n' > machinefile     ; time PyHST2_2017c  input.par   gpu2-1304,0"
# LAUNCHING_INSTRUCTION  = "time PyHST2_2018b  input.par scisoft13,0 |  tee output"
LAUNCHING_INSTRUCTION  = "PyHST2_2019b  input.par  |  tee output"

outputprefix="/data/scisofttmp/mirone/TEST_PYHST/RESULTS/slurm_2019b/"
# outputprefix="/data/scisofttmp/mirone/TEST_PYHST/RESULTS/2017c/2gpu_bis/tests"
# outputprefix="/data/scisofttmp/paleo/TEST_PYHST/DATASETS_RED/OUTPUTS"

inputprefix ="INPUTS"

PREFIX="/scisoft/users/mirone/WORKS/TEST_PYHST/DATASETS"


CASI_DISTRIBUTED = ["NNFBP",  "CRAYON",	"ID11_SNOW",  "LENA_MULTIRINGS",  "MULTIPAGANIN",  "PATCHES_VECTORIAL", "SIRT_LENA"]


CASI_ALL   = [ "BIG",  "CRAYON",  "HEIKKI",  "HELICOIDAL",  "ID11_SNOW",  "LENA",  "LENA_MULTIRINGS",  "MOUSSE",  "MULTIPAGANIN","NANOPOINTS",  "NNFBP",
"PATCHES_VECTORIAL",  "SINO_THRESHOLD",  "SIRT_LENA" ]


CASI_partial = [   "ID11_SNOW",  "LENA",  "LENA_MULTIRINGS",  "MOUSSE",  "MULTIPAGANIN","NANOPOINTS",  "NNFBP",
                   "PATCHES_VECTORIAL",  "SINO_THRESHOLD",  "SIRT_LENA" ]

casi = [  "ID11_SNOW"    ] 

##################################################
PREFIX="/home/mirone/WORKS/TEST_PYHST/DATASETS"
PREFIX="/scisoft/users/mirone/WORKS/TEST_PYHST/DATASETS"
casi=["CRAYON"]
LAUNCHING_INSTRUCTION  = "PyHST2_2019b  input.par  "
outputprefix="/home/mirone/TEST_PYHST/RESULTS/slurm_2019b/"
outputprefix="/data/scisofttmp/mirone/TEST_PYHST/RESULTS/slurm_2019b/"
###############################################################


import fnmatch
import os
import shutil
import string

ADATTA_INPUT=0
  
def adapt(l, prefix):
  pos = l.find("#")
  if pos>=0:
    l=l[:pos]
  pos = l.find("prefix")
  if pos==-1:
    return l
  if l[pos-1] in [" ","="] and l[pos+6]  in [" ","+" ]:
    pos1 = l.find("\"")
    pos2 = pos1+1+l[pos1+1:].find("\"")
    l = l[:pos] + prefix + l[pos1+1:pos2]
  return l

def getprefix(l):
  if "prefix"==l[:6]:
    exec(l)
    return prefix
    
def lancia_comando():
  #os.system("PyHST2_eli input.par scisoft12,0")
  if not ADATTA_INPUT:
    # os.system("echo 'localhost\nlocalhost\nlocalhost\nlocalhost\n' > machinefile     ; time PyHST2_2015d  input.par   gpu3-1801,0,1,2,3 | tee log.out")
    # os.system("echo 'localhost\n' > machinefile     ; time PyHST2_2015d  input.par   gpu3-1801,7")
    # os.system("echo 'localhost\n' > machinefile     ; time PyHST2_2015d  input.par   scisoft11,0")
    os.system( LAUNCHING_INSTRUCTION )
    # os.system("pyhst_2_2015d  input.par")
    # os.system("pyhst_2_2015f  input.par gpu2-1403,0")
    # os.system("PyHST2_2015f  input.par 2>stderr.log 1>stdlog.log ") #command 2> error 1> output
    # os.system("PyHST2_2015d  input.par ")
  else:
    # os.system("PyHST2_2015b  input_adapted.par")
    os.system("pyhst2 input.par")

matches = []
for root, dirnames, filenames in os.walk(inputprefix):
  for filename in fnmatch.filter(filenames, 'input.par'):
    matches.append(os.path.join(root, filename))
    
print matches

repertori = []


tmpmatches = matches
matches=[]

for caso in casi:
 for inp in tmpmatches:
  inpcut = inp[len(inputprefix)+1:]
  pL = inpcut.find("/")
  inpcut = inpcut[:pL]
  if inpcut == caso:
    matches.append(inp)
    print matches
  


for inp in matches:
  print inp
  pL = inp.find("/")
  pR = inp.rfind("/")
  direttorio = os.path.join(outputprefix,inp[pL+1:pR])
  filename   = inp[pR+1:]
  print direttorio
  print filename
  if not os.path.exists(direttorio):
    os.makedirs(direttorio)
    
  shutil.copy2(inp, os.path.join( direttorio  , filename  ))
  repertori.append(direttorio)


repertori.sort()
  
for d in repertori:
  savedPath = os.getcwd()
  os.chdir(d)
  if ADATTA_INPUT:
    S=""
    s=string.split(open("input.par","r").read(),"\n")
    gotprefix=0
    for l in s:
      if not gotprefix:
        prefix = getprefix(l)
        if prefix is not None:
          gotprefix=1
          continue
      if  gotprefix:
        l=adapt(l, prefix)
      S=S+l+"\n"
    open("input_adapted.par","w").write(S)
    print S

  if(1):
    s=open("input.par","r").read()
    s="PREFIX=\"%s\" \n"%PREFIX +s
    open("input.par","w").write(s)
    
  lancia_comando()
  os.chdir(savedPath)
  

def send_mail():

  if mailserver is not None:
    to = os.environ.get("USER").lower() + "@%s" %mailserver
    os.system("echo 'This is a notification that the reconstruction is done. The results are stored in " + str(savedPath) + "' | mail -s 'PyHST2 end of job notification' " + to)
  


send_mail()
  
  
  
  
  



