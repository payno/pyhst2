.. include:: texenv.txt

Wavelets regularization
========================

About
------

The wavelets regularization is a trade-off between Total Variation (TV) and Dictionary-based (DL) techniques :
    * TV is fast but mainly adapted to piecewise-constant images
    * DL yields better results, but is slower. Besides a dictionary has ideally to be learnt on a high quality reconstruction, which is not always available

The Wavelets technique is almost as fast as TV, and gives fairly good results without having to learn a dictionary -- most of natural images are sparse in a wavelet basis.
The basic idea is to promote the image sparsity in a Wavelets domain. Different families of wavelets exist, each having their own properties.


Using the Wavelets regularization
-----------------------------------

To use a Wavelets-based reconstruction, the following parameters are relevant :

.. code-block:: python

    ITERATIVE_CORRECTIONS = 300    # Number of iterations, have to be > 0
    DENOISING_TYPE = 8             # 8 for Wavelets
    W_WNAME = "haar"               # Wavelet to use. Mind the quotes
    W_LEVELS = 5                   # Number of decomposition levels
    W_CYCLE_SPIN = 1               # Enable random shifts
    W_SWT = 0                      # Plain DWT, no SWT


The parameter ``W_WNAME`` is the name of the Wavelet to use.
There are 72 available wavelets, the full list can be seen at `<http://wavelets.pybytes.com>`_.

The parameter ``W_LEVELS`` is the number of decomposition levels
The maximum level that can be reached is :math:`\log_2 \left( \frac{N}{h} \right)`, where :math:`N` is the image widh/height and :math:`h` the filter length (for example, 2 for Haar).
In practice, it is not always useful to use the maximum decomposition level.


The parameters ``W_CYCLE_SPIN`` and  ``W_SWT`` are related to translation invariance (see below).


Translation invariance
-----------------------

The standard Discrete Wavelets Transform (DWT) lacks a property called `translation invariance` :
given an image and a shifted version, the coefficients corresponding to the image and its shifted version differ notably.
This is detrimental to the denoising quality.

To achieve the shift invariance, two approaches can be used.
The first consists in solving the wavelet-regularized reconstruction problem with all different possible circular shifts of the image
(if :math:`N` is the width/height of the image, there are :math:`N^2` such shifts), and then averaging the results.
This of course is intractable. Instead, a strategy is to perform a random shift at each iteration of the reconstruction process.
At each iteration, the image undergoes a random circular shift (called `cycle spinning`), its coefficients are thresholded and back-transformed, and the shift is reverted.

A second approach consists in using the Stationary Wavelets Transform (SWT). This transform is not undersampled (there are more coefficients than DWT).

The following parameters can be set alternatively :

    * ``W_CYCLE_SPIN = 1`` enables the random shifts described above
    * ``W_SWT = 1`` uses the SWT instead of the DWT


These two options come with advantages and drawbacks :
    * DWT + cycle spin leads to substantially better results than plain DWT, but more iterations are needed so that the averaging effect can achieve an approximate translation invariance.
    * SWT yields exact translation invariance, hence better results, in a fewer iterations. But SWT has a larger memory footprint than DWT+cycle spin and one iteration of SWT is slightly slower than one iteration of SWT+cycle spin.






Available wavelets
-------------------

The filter names follow the Matlab/pywt convention.
Currently, 72 different wavelets are available, among the following families :

    * Haar : haar
    * Daubechies : db2, ..., db20
    * Symmetric : sym2, ..., sym20
    * Coifman : coif1, ..., coif5
    * Biorthogonal : bior1.3, bior1.5, bior2.2, bior2.4, bior2.6, bior2.8, bior3.1, bior3.3, bior3.5, bior3.7, bior3.9, bior4.4, bior5.5, bior6.8
    * Reverse-biorthogonal : rbio1.3, rbio1.5, rbio2.2, rbio2.4, rbio2.6, rbio2.8, rbio3.1, rbio3.3, rbio3.5, rbio3.7, rbio3.9, rbio4.4, rbio5.5, rbio6.8







