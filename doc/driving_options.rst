Driving Options
===============


 *===The following documentation has been extracted automatically from the comments found in the source code. Discard Parameters. object variable.*
  
 .. automodule:: Parameters_module
      :noindex:

 .. autoclass:: Parameters
       :members: OPTIONS, PENTEZONE, FBFILTER, SUMRULE, STRAIGTHEN_SINOS, BINNING, ZEROOFFMASK, OVERSAMPLING_FACTOR, VERBOSITY, TRYEDFCONSTANTHEADER, DOUBLEFFCORRECTION_ONTHEFLY, FF2_SIGMA, 

	       
 

..
   Contents:

   .. toctree::
      :maxdepth: 3

      volume_projection

   others
   ------
