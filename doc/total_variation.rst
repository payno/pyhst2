Total Variation
===============
  


This section deals with an iterative reconstruction algorithm (hence,
different from the filtered back projection), that solves the
minimization problem ::

 x = argmin 1/2||H x - y||^2  + beta_tv TV(x)

where:

  * x is the image to be reconstructed
  * H is the projection matrix
  * y is the sinogram
  * TV(x) is the total variation semi-norm of x, that is, the l1 norm of
    its gradient
  * beta_tv is a parameter controlling the relative importance of the two
    terms in the minimization

This algorithm tends to reconstruct piecewise-constant images. It is
therefore suitable for images with a limited number of phases, such as
many images in materials science. It is not suited, however, for images
with a very irregular texture, or for images with smooth large-scale
gradients.


 *The following documentation has been extracted automatically from the comments found in the source code. Discard Parameters. object variable.*
   

 .. automodule:: Parameters_module
     :noindex:
 .. autoclass:: Parameters
     :members: ITERATIVE_CORRECTIONS,DO_PRECONDITION, FISTA,DENOISING_TYPE,ITERATIVE_CORRECTIONS_NOPREC,BETA_TV, N_ITERS_DENOISING, DUAL_GAP_STOP, OPTIM_ALGORITHM

 
