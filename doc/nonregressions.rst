Non Regressions Tests
=====================

A tutorial on how to run non-regression test can be found here see :ref:`tutorial-nonregression`.

You can find in the sources that you can download from the git repository
:ref:`installation` 
a directory TEST_PYHST.

There you'll find a script *nonregression.py* that you
can run provided you edit it before hand
the variables indicating the location of datasets.

You can find a good set of dataset on the homepage
of the project https://forge.epn-campus.eu/projects/pyhst2
