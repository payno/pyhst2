Ring Correction with Thresholding
=================================

You find the the example in ::

/scisoft/ESRF_sw/TEST_PYHST/SINO_THRESHOLD


.. image:: datas/examples/threshold/nocorr.png
    :width:  300
    :height: 300


Input File:

.. literalinclude::  datas/examples/threshold/input_nocorr.par
    :language: python

.. image:: datas/examples/threshold/ringcorr.png
    :width:  300
    :height: 300


Input File:

.. literalinclude::  datas/examples/threshold/input_ringcorr.par
    :language: python

.. image:: datas/examples/threshold/threshold.png
    :width:  300
    :height: 300


Input File:

.. literalinclude::  datas/examples/threshold/input_threshold.par
    :language: python





.. image:: datas/examples/threshold/threshold_unsharp.png
    :width:  300
    :height: 300


Input File:

.. literalinclude::  datas/examples/threshold/input_threshold_unsharp.par
    :language: python

