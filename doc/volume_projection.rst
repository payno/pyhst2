Volume Projection
====================
This options allows to obtain, given a n already reconstructed volume,
the corresponding projections.
     
Here a concise description of the related input parameters.
  
 *The following documentation has been extracted automatically from the comments found in the source code. Discard Parameters. object variable.*
   

 .. automodule:: Parameters_module
    :noindex:

 .. autoclass:: Parameters
    :members:   STEAM_INVERTER, PROJ_OUTPUT_FILE, PROJ_OUTPUT_RESTRAINED

