.. _installation:

Installation
============

  * If you install from a Debian package you can skip the following points, install it , and then go directly to the code invocation section

  * Using Git, sources can be retrived with the following command ::

      git clone https://gitlab.esrf.fr/mirone/pyhst2

  * If you compile on a Debian system, for a local installation you can use, as an example  ::

	export TD=${PWD}/dummy/	
	python setup.py install --prefix ${TD}

    If your needs clang ::

      export USECLANG4NVCC=YES

    before compiling the code. You can possibly edit setup.py to change the extra parameters
    that are passed to nvcc in this case.

    PyHST2 has been tested with python3, python3, on intel and powerpc machines, and different CUDA versions.
    In case of very recente CUDA version, which have dismissed compute capabilities 2.0 you might have to remove
    the mentions to compute_20, from the setup.py, and possibly add new ones.

  * Then to run the code you must do beforehand ::

	export PYTHONPATH=${PWD}/dummy/lib/python2.7/site-packages
	export PATH=${PWD}/dummy/bin/:$PATH

    then go to the section Code Invocation to see how to run the code.
    You can disable compilation of CUDA modules by choosing the option in setup.py

  * If you generate a Debian package and install it, then it will be in the standard directories and you dont have to set PATH and PYTHONPATH


  * If you  have a non standard cuda installation ( if you are not on a Debian system )  you have to provide the cuda home ::

	export CUDAHOME=/usr/local/cuda-4.0.17

    for example

  * Examples :
        There is an archive containing datas for tests by anonymous ftp at ::

	  http://ftp.esrf.fr/pub/scisoft/PYHST2
	  
        by the way you'll find there also the documentation.
