.. pyhst2 documentation master file, created by
   sphinx-quickstart on Tue Dec 18 18:13:53 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyhst2's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 3

   installation
   invocation
   nonregressions
   input_format
   geometry
   output_format
   features
   ksvd
   unsharp3D
   driving_options
   performancetuning_options
   details_on_different_topics
   EXAMPLES
   tutorials

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

