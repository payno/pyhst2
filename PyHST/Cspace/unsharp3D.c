
#/*##########################################################################
   # Copyright (C) 2001-2013 European Synchrotron Radiation Facility
   #
   #              PyHST2
   #  European Synchrotron Radiation Facility, Grenoble,France
   #
   # PyHST2 is  developed at
   # the ESRF by the Scientific Software  staff.
   # Principal author for PyHST2: Alessandro Mirone.
   #
   # This program is free software; you can redistribute it and/or modify it
   # under the terms of the GNU General Public License as published by the Free
   # Software Foundation; either version 2 of the License, or (at your option)
   # any later version.
   #
   # PyHST2 is distributed in the hope that it will be useful, but WITHOUT ANY
   # WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   # FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
   # details.
   #
   # You should have received a copy of the GNU General Public License along with
   # PyHST2; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
   # Suite 330, Boston, MA 02111-1307, USA.
   #
   # PyHST2 follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
   # and cannot be used as a free plugin for a non-free program.
   #
   # Please contact the ESRF industrial unit (industry@esrf.fr) if this license
   # is a problem for you.
   #############################################################################*/
#undef NDEBUG
#include"Python.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include<time.h>

#include"structmember.h"
#include<string.h>
#include <unistd.h>
#include"cpyutils.h"
#include "py3c.h"

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION

#include"unsharp3D.h"

#define PY_ARRAY_UNIQUE_SYMBOL chst_ARRAY_API

#define cpyutils_PyArray1D_as_array(A,B,C) cpyutils_PyArray1D_as_array_TB( A,B,C, __FILE__, __LINE__ );
#define  cpyutils_o2i(A)  cpyutils_o2i_TB(A,  __FILE__, __LINE__  )
#define  cpyutils_o2f(A)  cpyutils_o2f_TB(A,  __FILE__, __LINE__  )

#include "numpy/arrayobject.h"

int mainunsharp(Unsharp_Para pars );


/*
 * The error object to expose
 */

static PyObject *ErrorObject;
#define onError(message)					\
  { PyErr_SetString(ErrorObject, message); return NULL;}

static PyObject *
unsharp3D( PyObject *self, PyObject *args)
{
  PyObject *OParameters;
  PyObject *Otmp;
  PyArrayObject *Oa;
  int i,k, dumint;


  if(!PyArg_ParseTuple(args,"O:unsharp3D",&(OParameters)
		       ))
    return NULL;
  Py_INCREF(OParameters);

  Unsharp_Para unsharp_para ; 

  unsharp_para.V3D_TIFFREAD = cpyutils_o2i (cpyutils_getattribute(OParameters ,"V3D_TIFFREAD" ) );
  unsharp_para.V3D_TIFFOUTPUT = cpyutils_o2i (cpyutils_getattribute(OParameters ,"V3D_TIFFOUTPUT" ) );
  unsharp_para.V3D_CONVZ  = cpyutils_o2i (cpyutils_getattribute(OParameters ,"V3D_CONVZ" ) );
  unsharp_para.V3D_FILENAMEPATTERN      = cpyutils_getstring(cpyutils_getattribute(OParameters ,"V3D_INPUTFILE") );
  unsharp_para.V3D_OUTPUTFILENAMEPATTERN = cpyutils_getstring(cpyutils_getattribute(OParameters ,"V3D_OUTPUTFILE") );
  
  unsharp_para.V3D_VOLDIMS   =  (int*) cpyutils_PyArray1D_as_array(cpyutils_getattribute(OParameters ,"V3D_VOLDIMS"),    &dumint, NPY_INT32 );
  assert(dumint==3);
  
  unsharp_para.V3D_ROICORNER =   (int*)   cpyutils_PyArray1D_as_array(cpyutils_getattribute(OParameters ,"V3D_ROICORNER"),  &dumint, NPY_INT32 ) ;
  assert(dumint==3);
  
  unsharp_para.V3D_ROISIZE   =    (int*)   cpyutils_PyArray1D_as_array(cpyutils_getattribute(OParameters ,"V3D_ROISIZE"), &dumint, NPY_INT32 ) ;
  assert(dumint==3);
  
  unsharp_para.V3D_PUS = cpyutils_o2f (cpyutils_getattribute(OParameters ,"V3D_PUS" ) );
  unsharp_para.V3D_PUC = cpyutils_o2f (cpyutils_getattribute(OParameters ,"V3D_PUC" ) );
  unsharp_para.V3D_GPUNUM = cpyutils_o2i (cpyutils_getattribute(OParameters ,"MYGPU" ) );
  
  mainunsharp( unsharp_para);
  return Py_None;
}


static PyMethodDef unsharp3D_functions[] = {
  {"unsharp3D", unsharp3D, METH_VARARGS, NULL },
  { NULL, NULL}
};

#if PY_MAJOR_VERSION >= 3
static struct PyModuleDef moduledef = {
    PyModuleDef_HEAD_INIT,  /* m_base */
    "Cspace",                 /* m_name */
    NULL,                   /* m_doc */
    -1,                     /* m_size */
     unsharp3D_functions           /* m_methods */
};
#endif


PyMODINIT_FUNC initunsharp3D(void)
{
  PyObject *m, *d;

#if PY_MAJOR_VERSION >= 3
  m = PyModule_Create(&moduledef);
#else
  m = Py_InitModule("unsharp3D", unsharp3D_functions);
#endif
  
  


  
  d = PyModule_GetDict(m);
  ErrorObject = Py_BuildValue("s","unsharp3D.error");
  PyDict_SetItemString(d,"error", ErrorObject);
  if(PyErr_Occurred())
    Py_FatalError("can't initialize module unsharp3D");
  
#ifdef import_array
  import_array();
#endif
  
}
