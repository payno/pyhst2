#############################################################################*/

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


import string


from .  import string_six


import traceback
import sys
import numpy
import math
import time

from os.path import basename
import os.path



import mpi4py.MPI as MPI


from . import setCpuSet
from . import segment_cy



def segmenta():

    myrank = MPI.COMM_WORLD.Get_rank()
    nprocs = MPI.COMM_WORLD.Get_size()
    mypname = MPI.Get_processor_name()
    comm = MPI.COMM_WORLD
    
    maxnargs = 8
    mygpus, MemPerProc,coresperproc,ncpusperproc =setCpuSet.setCpuSet(maxnargs)
    print( " PROCESS :mypname, myrank, nprocs, coresperproc, mygpus, MemPerProc , argv " , mypname, myrank, nprocs, coresperproc, mygpus, MemPerProc, sys.argv)
    
    pagbone_outputfile     = sys.argv[2]
    threshold              = string.atof(sys.argv[3])
    medianR                = string.atof(sys.argv[4])
    maskbone_outputfile    = sys.argv[5]
    dilatation             = string.atoi(sys.argv[6])
    
    from xml.dom import minidom
    xmldoc = minidom.parse(pagbone_outputfile+'.xml')
    SIZEZ = string.atoi(xmldoc.getElementsByTagName('SIZEZ')[0].firstChild.data)
    SIZEY = string.atoi(xmldoc.getElementsByTagName('SIZEY')[0].firstChild.data)
    SIZEX = string.atoi(xmldoc.getElementsByTagName('SIZEX')[0].firstChild.data)
    
    
    if myrank==0     :
        size = SIZEZ*SIZEY*SIZEX
        f=open(maskbone_outputfile,"w")
        f.seek(size*4-1)
        f.write(" ")
        f.close()

    comm.Barrier()
    print( " chiamo cython ", myrank)
    segment_cy.pysegment( pagbone_outputfile,
                          threshold,
                          medianR,
                          maskbone_outputfile,
                          myrank,
                          nprocs, 
                          coresperproc,
                          MemPerProc  ,
                          SIZEZ, SIZEY, SIZEX, dilatation)
    
def spilla():

    myrank = MPI.COMM_WORLD.Get_rank()
    nprocs = MPI.COMM_WORLD.Get_size()
    mypname = MPI.Get_processor_name()
    comm = MPI.COMM_WORLD
    
    maxnargs = 8
    mygpus, MemPerProc,coresperproc,ncpusperproc=setCpuSet.setCpuSet(maxnargs)
    print( " PROCESS :mypname, myrank, nprocs, coresperproc, mygpus, MemPerProc , argv " , mypname, myrank, nprocs, coresperproc, mygpus, MemPerProc, sys.argv)
    

    pagbone_outputfile     = sys.argv[2]
    absbone_outputfile     = sys.argv[3]
    maskbone_outputfile    = sys.argv[4]
    threshold              = string.atof(sys.argv[5])
    corrbone_outputfile    = sys.argv[6]

    from xml.dom import minidom
    xmldoc = minidom.parse(absbone_outputfile+'.xml')
    SIZEZ = string.atoi(xmldoc.getElementsByTagName('SIZEZ')[0].firstChild.data)
    SIZEY = string.atoi(xmldoc.getElementsByTagName('SIZEY')[0].firstChild.data)
    SIZEX = string.atoi(xmldoc.getElementsByTagName('SIZEX')[0].firstChild.data)
    
    
    if myrank==0     :
        size = SIZEZ*SIZEY*SIZEX
        f=open(corrbone_outputfile,"w")
        f.seek(size*4-1)
        f.write(" ")
        f.close()

    comm.Barrier()
    print( absbone_outputfile, pagbone_outputfile, maskbone_outputfile,threshold,corrbone_outputfile)
    segment_cy.pyspilla( pagbone_outputfile,
                         absbone_outputfile,
                         maskbone_outputfile ,
                         threshold,
                         corrbone_outputfile,
                         myrank,
                         nprocs, 
                         coresperproc,
                         MemPerProc  ,
                         SIZEZ, SIZEY, SIZEX)

def incolla():

    myrank = MPI.COMM_WORLD.Get_rank()
    nprocs = MPI.COMM_WORLD.Get_size()
    mypname = MPI.Get_processor_name()
    comm = MPI.COMM_WORLD
    
    maxnargs = 7
    mygpus, MemPerProc,coresperproc,ncpusperproc =setCpuSet.setCpuSet(maxnargs)
    print( " PROCESS :mypname, myrank, nprocs, coresperproc, mygpus, MemPerProc , argv " , mypname, myrank, nprocs, coresperproc, mygpus, MemPerProc, sys.argv)
    
    pagbone_outputfile     = sys.argv[2]
    maskbone_outputfile    = sys.argv[3]
    correctedvol_outputfile    = sys.argv[4]
    outputfile    = sys.argv[5]

    from xml.dom import minidom
    xmldoc = minidom.parse(pagbone_outputfile+'.xml')
    SIZEZ = string.atoi(xmldoc.getElementsByTagName('SIZEZ')[0].firstChild.data)
    SIZEY = string.atoi(xmldoc.getElementsByTagName('SIZEY')[0].firstChild.data)
    SIZEX = string.atoi(xmldoc.getElementsByTagName('SIZEX')[0].firstChild.data)
    
    
    if myrank==0     :
        size = SIZEZ*SIZEY*SIZEX
        f=open(outputfile,"w")
        f.seek(size*4-1)
        f.write(" ")
        f.close()

    comm.Barrier()
    
    if myrank==0     :
        name1 = correctedvol_outputfile+".info"
        name2 = outputfile+".info"
        open(name2,"w").write(open(name1,"r").read())
        
        name1 = correctedvol_outputfile+".xml"
        name2 = outputfile+".xml"
        name2_base = os.path.basename(outputfile)
        s = open(name1,"r").read()
        pos1=s.find("<SUBVOLUME_NAME>")
        pos2=s.find("<SIZEX>")
        s = s[:pos1] + "<SUBVOLUME_NAME>%s</SUBVOLUME_NAME>"%name2_base   + s[pos2:]
        open(name2,"w").write(s)

    comm.Barrier()
        

    segment_cy.pyincolla( pagbone_outputfile,
                          maskbone_outputfile,
                          correctedvol_outputfile,
                          outputfile,
                          myrank,
                          nprocs, 
                          coresperproc,
                          MemPerProc  ,
                          SIZEZ, SIZEY, SIZEX)



if(sys.argv[1]=="segmenta") :
    segmenta()
if(sys.argv[1]=="spilla") :
    spilla()
if(sys.argv[1]=="incolla") :
    incolla()

