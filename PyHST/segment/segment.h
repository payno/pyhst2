void  segment( char *pagbone_outputfile,
	       float threshold,
	       int medianR,
	       char *maskbone_outputfile,
	       int myrank,
	       int ncpus,
	       int nprocs,
	       long int MemPerProc, int SIZEZ, int SIZEY, int SIZEX, int dilatation);
 
void  spilla ( char *pagbone_outputfile,
	       char *absbone_outputfile,
	       char *maskbone_outputfile,
	       float threshold,
	       char *corrbone_outputfile,
	       int myrank,
	       int nprocs,
	       int ncpus,
	       long int MemPerProc, int SIZEZ, int SIZEY, int SIZEX);

void   incolla( char *pagbone_outputfile,
		char *maskbone_outputfile,
		char *correctedvol_outputfile,         
		char *outputfile,
		int myrank,
		int nprocs,
		int ncpus,
		long int MemPerProc, int SIZEZ, int SIZEY, int SIZEX);


