# -*- coding: utf-8 -*-
###################################################################################
# Fits, Elastic constants fits : Alessandro Mirone
# European Synchrotron Radiation Facility
###################################################################################
#distutils: extra_compile_args = -fopenmp
# distutils: language = c++

from __future__ import print_function

import cython
import numpy
cimport numpy
from libc.stdio cimport printf
 
#  pagbone_outputfile, threshold, maskbone_outputfile,   myrank, nprocs, ncpus, MemPerProc
cdef extern from "segment.h":
 cdef void  segment(
     char *,
     float,
     int,
     char *,
     int,
     int,
     int,
     long int,
     int , int , int ,int) nogil
 
cdef extern from "segment.h":
 cdef  void  spilla ( char *,
                      char *,
                      char *,
                      float ,
                      char *,
                      int ,
                      int ,
                      int ,
                      long int ,
                      int , int , int ) nogil
 
cdef extern from "segment.h":
 cdef  void  incolla ( char *,
	       char *,
	       char *,
	       char *,
	       int ,
	       int ,
	       int ,
	       long int ,
               int , int , int ) nogil
 

@cython.boundscheck(False)
def pysegment( pagbone_outputfile_py,
               float threshold,
	       int medianR,
               maskbone_outputfile_py,
               int myrank,
               int nprocs,
               int ncpus,
               long int MemPerProc,
               int SIZEZ, int SIZEY, int SIZEX, int dilatation) :

    print (" NCPUS ", ncpus)
    cdef bytes py_bytes = pagbone_outputfile_py.encode()
    cdef char* pagbone_outputfile   = py_bytes

    cdef py_bytes2 = maskbone_outputfile_py.encode()
    cdef char* maskbone_outputfile  = py_bytes2

    segment( pagbone_outputfile,
             threshold,
             medianR,
	     maskbone_outputfile,
             myrank,
	     nprocs, 
	     ncpus,
	     MemPerProc,
             SIZEZ, SIZEY, SIZEX,  dilatation) 



@cython.boundscheck(False)
def pyspilla(     pagbone_outputfile_py,
                  absbone_outputfile_py,
                  maskbone_outputfile_py,
                  float threshold,
                  corrbone_outputfile_py,
                  int myrank,
                  int nprocs,
                  int ncpus,
                  long int MemPerProc,
                  int SIZEZ, int SIZEY, int SIZEX) :
    
    print( " NCPUS ", ncpus)


    cdef bytes py_bytes0 = pagbone_outputfile_py.encode()
    cdef char* pagbone_outputfile   = py_bytes0

    cdef bytes py_bytes = absbone_outputfile_py.encode()
    cdef char* absbone_outputfile   = py_bytes


    print( " ------------ " )
    print( absbone_outputfile_py)
    print( " ------------ " )
    printf(" %s \n", absbone_outputfile  );

    cdef  py_bytes2 = maskbone_outputfile_py.encode()
    cdef char* maskbone_outputfile  = py_bytes2

    cdef py_bytes3 = corrbone_outputfile_py.encode()
    cdef char* corrbone_outputfile  = py_bytes3

    print( " ------------ " )
    print( absbone_outputfile_py)
    print( " ------------ " )
    printf(" %s \n", absbone_outputfile  );
    
    spilla( pagbone_outputfile,
            absbone_outputfile,
            maskbone_outputfile,
            threshold,
            corrbone_outputfile,
            myrank,
            nprocs, 
            ncpus,
            MemPerProc,
            SIZEZ, SIZEY, SIZEX )


@cython.boundscheck(False)
def pyincolla( pagbone_outputfile_py,     
               maskbone_outputfile_py,    
               correctedvol_outputfile_py,
               outputfile_py,             
               int myrank,
               int nprocs,
               int ncpus,
               long int MemPerProc,
               int SIZEZ, int SIZEY, int SIZEX) :
    
    print( " NCPUS ", ncpus)
    cdef bytes py_bytes = pagbone_outputfile_py.encode()
# cdef char* pagbone_outputfile_py  = py_bytes
    cdef char* pagbone_outputfile   = py_bytes

    cdef py_bytes2 = maskbone_outputfile_py.encode()
    cdef char* maskbone_outputfile  = py_bytes2

    cdef py_bytes3 = correctedvol_outputfile_py.encode()
    cdef char* correctedvol_outputfile  = py_bytes3

    cdef py_bytes4 = outputfile_py.encode()
    cdef char* outputfile  = py_bytes4
    
    incolla( pagbone_outputfile,     
             maskbone_outputfile,    
             correctedvol_outputfile,
             outputfile,             
             myrank,
             nprocs, 
             ncpus,
             MemPerProc,
             SIZEZ, SIZEY, SIZEX )
    
